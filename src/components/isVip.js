import { useField, useFormikContext } from 'formik';
import clsx from 'clsx';


const IsVip = ({ children, ...props }) => {
	const [field] = useField({ ...props, type: 'checkbox' });
	const { values } = useFormikContext();

	return (
		<div className="flex items-center gap-4">
			<h3 className="text-yellow-400">{children}</h3>
			<label  className={
				'w-14 h-8 relative cursor-pointer rounded-full flex items-center p-1 duration-500 bg-black'}>
				<div className={clsx('checkbox-input bg-yellow-400 transform absolute duration-500 w-6 h-6 flex rounded-full ',
					values.vip ? ' translate-x-6' : 'translate-x-0')}>
					<input type="checkbox" className="hidden" {...field} {...props}/>
				</div>
			</label>
		</div>
	);
};

export default IsVip;
