import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import IsVip from './isVip';
import Input from './input';
import axios from 'axios';

const BuyTicket = ({ isVip, chosenShowingId }) => {
	return (
		<Formik
			initialValues={{
				firstName: '',
				lastName: '',
				showing: chosenShowingId,
				vip: isVip,
			}}
			validationSchema={Yup.object({
				firstName: Yup.string()
				.max(50, 'Maksymalnie 50 znaków')
				.required('Wymagane'),
				lastName: Yup.string()
				.max(50, 'Maksymalnie 50 znaków')
				.required('Wymagane'),
			})}
			onSubmit={(values) => {
				setTimeout(() => {
					axios.post('http://localhost:8080/ticket', values)
					.then(response => {
						console.log(response)
						window.location.replace(`http://localhost:8080/tickets/bilet_${response.data.success}.pdf`);
						})
					.catch(error => console.log(error))
				}, 400);
			}}>
			{({ values}) => (
				<Form>
					<div style={{backgroundImage: `url(/form-grafic.png)`}} className="flex bg-cover flex-col items-center w-5/12 py-36 mx-auto mt-20 bg-blue px-12">
						<div className="flex flex-col gap-3">
							<Input
								name="firstName"
								type="text"
								placeholder="Imię"/>
							<Input
								name="lastName"
								type="text"
								placeholder="Nazwisko"/>
							<div className="mt-1 mb-8 self-end">
								<IsVip
									name="vip"
									checked={values.vip}>
									VIP
								</IsVip>
							</div>
								<button type="submit" className="bg-red-900 self-end rounded-3xl w-40 py-4 text-white">Kup bilet</button>
						</div>
					</div>
				</Form>
			)}
		</Formik>
	);
};

export default BuyTicket;
