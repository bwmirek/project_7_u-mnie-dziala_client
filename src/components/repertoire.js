import { Swiper, SwiperSlide } from 'swiper/react';
import Film from './film.js';
import 'swiper/swiper-bundle.css';
import { useEffect, useState } from 'react';

const Repertoire = ({ choseFilm }) => {
	const [availableMovies, setAvailableMovies] = useState(null);

	useEffect(() => {
		const fetchData = async () => {
			const response = await fetch('http://localhost:8080/movies', { method: 'GET' });
			const data = await response.json();
			setAvailableMovies(data);
		};
		fetchData().then();
	}, []);

	return (
		<div className="h-full pt-40">
			<Swiper
				slidesPerView={2}
				centeredSlides={true}>
				{availableMovies && (
					availableMovies.map(({ id, title, description, cover, rating, price }) => (
						<SwiperSlide key={id}>
							<Film choseFilm={choseFilm} availableMovies={availableMovies} title={title} description={description} cover={cover} rating={rating} price={price}/>
						</SwiperSlide>
					))
				)}
			</Swiper>
		</div>
	);
};

export default Repertoire;
