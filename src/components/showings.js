import { useEffect, useState } from 'react';
import { Swiper, SwiperSlide } from 'swiper/react';
import ShowingsComponent from './showingsComponent';
import 'swiper/swiper-bundle.css';

const Showings = ({ pickedFilm, setVip, setChosenShowingId }) => {
	const [availableShowings, setAvailableShowings] = useState([]);
	let filteredShowings = availableShowings.filter(item => item.movie === pickedFilm[0].id);

	useEffect(() => {
		const fetchData = async () => {
			const response = await fetch('http://localhost:8080/showings', { method: 'GET' });
			const data = await response.json();
			setAvailableShowings(data);
		};
		fetchData().then();
	}, []);

	return (
		<div>
			<Swiper
				slidesPerView={2}
				centeredSlides={true}
				spaceBetween={0}
				direction='vertical'>
				{availableShowings && (filteredShowings.map(({ id, seats, vipSeats, tickets, vipTickets, playingDate }) => (
					<SwiperSlide key={id}>
						<ShowingsComponent
							id={id} setVip={setVip} seats={seats} tickets={tickets} vipSeats={vipSeats} vipTickets={vipTickets} playingDate={playingDate} pickedFilm={pickedFilm} setChosenShowingId={setChosenShowingId}/>
					</SwiperSlide>
				)))}
			</Swiper>
		</div>
	)
};

export default Showings;
