import './App.css';
import curtain from './curtain.png'
import Repertoire from './components/repertoire';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { useState } from 'react';
import Showings from './components/showings'
import BuyTicket from './components/buyTicket';

function App() {
  const [pickedFilm, choseFilm] = useState([])
  const [isVip, setVip] = useState(false)
  const [chosenShowingId, setChosenShowingId] = useState(0)

  return (
    <Router>
      <div className="relative overflow-hidden bg-gradient-to-r from-black via-gray-600 to-black w-screen h-screen">
        <img src={curtain} alt="Kurtyna" className="absolute z-50 w-screen h-screen pointer-events-none"/>
        <div className="">
          <Switch>
            <Route exact path='/'>
              <Repertoire choseFilm={choseFilm}/>
            </Route>
            <Route path='/showings'>
              <Showings pickedFilm={pickedFilm} setVip={setVip} setChosenShowingId={setChosenShowingId}/>
            </Route>
            <Route path='/buyTicket'>
              <BuyTicket isVip={isVip} chosenShowingId={chosenShowingId}/>
            </Route>
          </Switch>
        </div>
      </div>
    </Router>
  );
}

export default App;
